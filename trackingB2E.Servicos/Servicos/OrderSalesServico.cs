﻿using Amazon;
using Amazon.Runtime.Internal;
using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using trackingB2E.Dominio.Entidades;
using trackingB2E.Repositorio.Interfaces;
using trackingB2E.Servicos.Interfaces;

namespace trackingB2E.Servicos.Servicos
{
    public class OrderSalesServico : IOrderSalesServico
    {
        private readonly IConfiguration _configuration;
        private readonly IAmazonSQS _sqs;
        private readonly IConsumeRepositorio _consumeRepositorio;

        public OrderSalesServico(IConfiguration configuration, IConsumeRepositorio consumeRepositorio)
        {
            _configuration = configuration;
            var AccessKey = _configuration.GetSection("CredentialProfileOptions").GetSection("AccessKey").Value;
            var SecretKey = _configuration.GetSection("CredentialProfileOptions").GetSection("SecretKey").Value;

            _sqs = new AmazonSQSClient(AccessKey, SecretKey, RegionEndpoint.USEast2);

            _consumeRepositorio = consumeRepositorio;
        }

        public void SetOrderSalesFila(OrderSales orderSales)
        {
            try
            {
                SendMessageRequest sendMessage = new SendMessageRequest
                {
                    QueueUrl = _configuration.GetSection("AwsSQSQueueUrls").GetSection("SalesOrderQueueUrl").Value,
                    MessageBody = JsonConvert.SerializeObject(orderSales)
                };
                _sqs.SendMessageAsync(sendMessage);
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - erro ao inserir na fila SalesOrderQueue");

                throw detalheExcecao;
            }


        }

        #region Entidade
        public OrderSales GetOrderSalesByClientOrderId(string orderId)
        {
            var orderSales = _consumeRepositorio.GetOrderSalesByClientOrderId(orderId);
            return orderSales;
        }

        public OrderSales GetOrderSalesBySapOrderId(string orderId)
        {
            var orderSales = _consumeRepositorio.GetOrderSalesBySapOrderId(orderId);
            return orderSales;
        }

        public OrderSales GetOrderSalesBySalesForceOrderId(string orderId)
        {
            var orderSales = _consumeRepositorio.GetOrderSalesBySalesForceOrderId(orderId);
            return orderSales;
        }
        #endregion

        #region Hearder Status
        public Status GetHeaderOrderSalesByClientOrderId(string orderId)
        {
            var orderSales = _consumeRepositorio.GetHeaderOrderSalesByClientOrderId(orderId);
            return orderSales;
        }

        public Status GetHeaderOrderSalesBySapOrderId(string orderId)
        {
            var orderSales = _consumeRepositorio.GetHeaderOrderSalesBySapOrderId(orderId);
            return orderSales;
        }

        public Status GetHeaderOrderSalesBySalesForceOrderId(string orderId)
        {
            var orderSales = _consumeRepositorio.GetHeaderOrderSalesBySalesForceOrderId(orderId);
            return orderSales;
        }
        #endregion

        #region Itens

        #region All Itens
        public List<Itens> GetAllItensOrderSalesByClientOrderId(string orderId)
        {
            var orderSalesItens = _consumeRepositorio.GetAllItensOrderSalesByClientOrderId(orderId);
            return orderSalesItens;
        }

        public List<Itens> GetAllItensOrderSalesBySapOrderId(string orderId)
        {
            var orderSalesItens = _consumeRepositorio.GetAllItensOrderSalesBySapOrderId(orderId);
            return orderSalesItens;
        }

        public List<Itens> GetAllItensOrderSalesBySalesForceOrderId(string orderId)
        {
            var orderSalesItens = _consumeRepositorio.GetAllItensOrderSalesBySalesForceOrderId(orderId);
            return orderSalesItens;
        }
        #endregion

        #region All Status Item
        public List<Status> GetAllStatusItensOrderSalesByClientOrderId(string orderId, string orderItemId)
        {
            var orderSalesItemStatus = _consumeRepositorio.GetAllStatusItensOrderSalesByClientOrderId(orderId, orderItemId);
            return orderSalesItemStatus;
        }

        public List<Status> GetAllStatusItensOrderSalesBySapOrderId(string orderId, string orderItemId)
        {
            var orderSalesItemStatus = _consumeRepositorio.GetAllStatusItensOrderSalesBySapOrderId(orderId, orderItemId);
            return orderSalesItemStatus;
        }

        public List<Status> GetAllStatusItensOrderSalesBySalesForceOrderId(string orderId, string orderItemId)
        {
            var orderSalesItemStatus = _consumeRepositorio.GetAllStatusItensOrderSalesBySalesForceOrderId(orderId, orderItemId);
            return orderSalesItemStatus;
        }
        #endregion

        #region Get Item 
        public Itens GetItensOrderSalesByClientOrderId(string orderId, string orderItemId)
        {
            var orderSalesItem = _consumeRepositorio.GetItensOrderSalesByClientOrderId(orderId, orderItemId);
            return orderSalesItem;
        }

        public Itens GetItensOrderSalesBySapOrderId(string orderId, string orderItemId)
        {
            var orderSalesItem = _consumeRepositorio.GetItensOrderSalesBySapOrderId(orderId, orderItemId);
            return orderSalesItem;
        }

        public Itens GetItensOrderSalesBySalesForceOrderId(string orderId, string orderItemId)
        {
            var orderSalesItem = _consumeRepositorio.GetItensOrderSalesBySapOrderId(orderId, orderItemId);
            return orderSalesItem;
        }
        #endregion 

        #region Last Status Item
        public Status GetLastStatusItensOrderSalesByClientOrderId(string orderId, string orderItemId)
        {
            var orderSalesItemLastStatus = _consumeRepositorio.GetLastStatusItensOrderSalesByClientOrderId(orderId, orderItemId);
            return orderSalesItemLastStatus;
        }

        public Status GetLastStatusItensOrderSalesBySapOrderId(string orderId, string orderItemId)
        {
            var orderSalesItemLastStatus = _consumeRepositorio.GetLastStatusItensOrderSalesBySapOrderId(orderId, orderItemId);
            return orderSalesItemLastStatus;
        }

        public Status GetLastStatusItensOrderSalesBySalesForceOrderId(string orderId, string orderItemId)
        {
            var orderSalesItemLastStatus = _consumeRepositorio.GetLastStatusItensOrderSalesBySalesForceOrderId(orderId, orderItemId);
            return orderSalesItemLastStatus;
        }
        #endregion 

        #endregion
    }
}

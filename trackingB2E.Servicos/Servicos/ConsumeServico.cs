﻿using Amazon;
using Amazon.Runtime.CredentialManagement;
using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using trackingB2E.Dominio.Entidades;
using trackingB2E.Repositorio.Interfaces;
using trackingB2E.Servicos.Interfaces;

namespace trackingB2E.Servicos.Servicos
{
    public class ConsumeServico : IConsumeServico
    {
        private readonly IConfiguration _configuration;
        private readonly IAmazonSQS _sqs;
        private readonly IRepositorio _repositorio;
        
        
        public ConsumeServico(IConfiguration configuration, IRepositorio repositorio)
        {
            _configuration = configuration;
            var AccessKey = _configuration.GetSection("CredentialProfileOptions").GetSection("AccessKey").Value;
            var SecretKey = _configuration.GetSection("CredentialProfileOptions").GetSection("SecretKey").Value;
            
            _sqs = new AmazonSQSClient(AccessKey, SecretKey, RegionEndpoint.USEast2);
            _repositorio = repositorio;
        }

        public async void ConsumeOrderSalesFila()
        {
            var queue = _configuration.GetSection("AwsSQSQueueUrls").GetSection("SalesOrderQueueUrl").Value;

            List<Message> messages = _sqs.ReceiveMessageAsync(queue).Result.Messages;

            foreach (var item in messages)
            {
                var orderSalesLst = JsonConvert.DeserializeObject<List<OrderSales>>(item.Body);

                foreach (var orderSales in orderSalesLst)
                {
                    //To do verifica se já existe no banco 
                    var orderExist = VerificaEntidade(orderSales);

                    if (orderExist.Equals(null))
                    {
                        //Se não cadastra
                        SaveNewOrder(orderSales);
                        //gravar no mongoDB
                    }
                    /*
                    else
                    {
                        //Se sim atualiza
                        UpdateOrder(orderSales);
                        //update no mongoDB
                    }
                    */
                }

                await _sqs.DeleteMessageAsync(queue, item.ReceiptHandle);
            }
        }

        public async void ConsumeSAPOrderFila()
        {
            var queue = _configuration.GetSection("AwsSQSQueueUrls").GetSection("SAPQueueUrl").Value;
            
            List<Message> messages = _sqs.ReceiveMessageAsync(queue).Result.Messages;

            foreach (var item in messages)
            {
                var orderLst = JsonConvert.DeserializeObject<List<Order>>(item.Body);

                foreach (var order in orderLst)
                {
                    OrderSales auxOrder = new OrderSales
                    {
                        sapSalesOrderNumber = order.SAPSalesOrderNumber,
                        Date = order.Date
                        
                    };

                    auxOrder.Status = new List<Status>();
                    auxOrder.itens = new List<Itens>();

                    auxOrder.Status.Add(new Status()
                    {
                        StatusCode = order.StatusCode,
                        StatusName = order.StatusName,
                        Date = order.Date
                    });


                    //To do verifica se já existe no banco 
                    var orderExist = VerificaEntidade(auxOrder);

                    if (orderExist.Result == null)
                    {
                        //Se não cadastra
                        SaveNewOrder(auxOrder);
                        //gravar no mongoDB
                    }
                    else
                    {
                        //Verificar se o Status já existe

                        var orderStatusExist = VerificaEntidadeStatus(auxOrder);

                        if (orderStatusExist.Result == null)
                        {
                            UpdateOrder(auxOrder);
                            //update no mongoDB
                        }
                    }
                }
                await _sqs.DeleteMessageAsync(queue, item.ReceiptHandle);
            }
        }

        public async void ConsumeSAPOrderItensFila()
        {
            var queue = _configuration.GetSection("AwsSQSQueueUrls").GetSection("SAPOrderItensQueueUrl").Value;

            List<Message> messages = _sqs.ReceiveMessageAsync(queue).Result.Messages;
            
            foreach (var msg in messages)
            {   
                var orderItemLst = JsonConvert.DeserializeObject<List<OrderItem>>(msg.Body);

                foreach (var item in orderItemLst)
                {
                    var orderItem = new OrderSales()
                    {
                        sapSalesOrderNumber = item.SAPSalesOrderNumber,
                        Date = item.Date
                        
                    };

                    orderItem.itens = new List<Itens>();
                    orderItem.itens.Add(new Itens()
                    {
                        SAPSalesOrderNumber = item.SAPSalesOrderNumber,
                        ItemNumber = item.ItemNumber,
                        Date = item.Date,
                        Status = new List<Status>(){
                            new Status(){
                            
                                StatusCode = item.StatusCode,
                                StatusName = item.StatusName,
                                Date = item.Date
                            }
                        }
                        
                    });


                    //To do verifica se já existe no banco 
                    var orderItemExist = VerificaEntidadeItem(item);

                    if (orderItemExist.Result == null)
                    { 
                        //Se não cadastra
                        UpdateOrderItem(orderItem);
                        //To do gravar no mongoDB
                    }
                    else
                    {
                        //Verifica se o Status do item precisa ser atualizado
                        var orderItemStatusExist = VerificaEntidadeItemStatus(item);
                        //Se sim atualiza
                        if (orderItemStatusExist.Result == null)
                        {
                            UpdateOrderItemStatus(orderItem);
                            //To do update no mongoDB
                        }

                    }

                }
                
                await _sqs.DeleteMessageAsync(queue, msg.ReceiptHandle);
            }
        }

        private bool SaveNewOrder(OrderSales order)
        {
            try
            {
                var retorno = _repositorio.InsertOrderSalesMongo(order);

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao tentar inserir o pedido {order.originSalesOrderNumber}");

                throw detalheExcecao;
            }
        }

        private async Task<OrderSales> VerificaEntidade(OrderSales order)
        {
            try
            {
                var retorno = await _repositorio.BuscaOrderExistente(order);
                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar a existência do pedido {order.originSalesOrderNumber}");

                throw detalheExcecao;
            }

        }

        private async Task<OrderSales> VerificaEntidadeStatus(OrderSales order)
        {
            try
            {
                var retorno = await _repositorio.BuscaOrderStatusExistente(order);
                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar a existência do status no pedido {order.originSalesOrderNumber}");

                throw detalheExcecao;
            }

        }

        private async Task<OrderSales> VerificaEntidadeItem(OrderItem orderItem)
        {
            try
            {
                var retorno = await _repositorio.BuscaOrderItemExistente(orderItem);
                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar a existência do item no pedido {orderItem.SAPSalesOrderNumber}");

                throw detalheExcecao;
            }

        }

        private async Task<OrderSales> VerificaEntidadeItemStatus(OrderItem orderItem)
        {
            try
            {
                var retorno = await _repositorio.BuscaOrderItemStatusExistente(orderItem);
                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar a existência do status {orderItem.StatusName} do item {orderItem.ItemNumber} no pedido {orderItem.SAPSalesOrderNumber}");

                throw detalheExcecao;
            }

        }


        

        private bool UpdateOrder(OrderSales order)
        {
            try
            {
                var retorno = _repositorio.AtualizarOrderSalesMongo(order);

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao tentar atualizar o pedido {order.originSalesOrderNumber}");

                throw detalheExcecao;
            }
        }

        private bool UpdateOrderItem(OrderSales orderItem)
        {
            try
            {
                var retorno = _repositorio.AtualizarOrderSalesItemMongo(orderItem);

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao tentar atualizar o item {orderItem.itens} do pedido {orderItem.sapSalesOrderNumber}");

                throw detalheExcecao;
            }
        }

        private bool UpdateOrderItemStatus(OrderSales orderItem)
        {
            try
            {
                var retorno = _repositorio.AtualizarOrderSalesItemStatusMongo(orderItem);

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao tentar atualizar o status do item {orderItem.itens} do pedido {orderItem.sapSalesOrderNumber}");

                throw detalheExcecao;
            }
        }
    }
}

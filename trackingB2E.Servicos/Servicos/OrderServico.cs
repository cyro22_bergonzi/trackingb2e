﻿using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using trackingB2E.Dominio.Entidades;
using trackingB2E.Servicos.Interfaces;

namespace trackingB2E.Servicos.Servicos
{
    public class OrderServico : IOrderServico
    {
        private readonly IConfiguration _configuration;
        private readonly IAmazonSQS _sqs ; 
        public OrderServico(IConfiguration configuration)
        {
            _configuration = configuration;
            var AccessKey = _configuration.GetSection("CredentialProfileOptions").GetSection("AccessKey").Value;
            var SecretKey = _configuration.GetSection("CredentialProfileOptions").GetSection("SecretKey").Value;

            _sqs = new AmazonSQSClient(AccessKey, SecretKey, RegionEndpoint.USEast2);
        }

        public void SetHeaderOrderSAPFila(SapSalesOrderHeader order)
        {
            try
            {
                SendMessageRequest sendMessage = new SendMessageRequest
                {
                    QueueUrl = _configuration.GetSection("AwsSQSQueueUrls").GetSection("SAPQueueUrl").Value,
                    MessageBody = JsonConvert.SerializeObject(order.SAPOrderHeader)
                };
                _sqs.SendMessageAsync(sendMessage);
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - erro ao inserir na fila SAPQueue");

                throw detalheExcecao;
            }
        }
    }
}

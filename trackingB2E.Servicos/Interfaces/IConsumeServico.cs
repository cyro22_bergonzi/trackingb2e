﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using trackingB2E.Dominio.Entidades;

namespace trackingB2E.Servicos.Interfaces
{
    public interface IConsumeServico
    {
        void ConsumeSAPOrderFila();
        void ConsumeSAPOrderItensFila();
        void ConsumeOrderSalesFila();

    }
}

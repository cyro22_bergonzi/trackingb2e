﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace trackingB2E.Dominio.Entidades
{
    public class OrderItem
    {
        [BsonElement]
        public string SAPSalesOrderNumber { get; set; }
        [BsonElement]
        public string ItemNumber { get; set; }
        [BsonElement]
        public string StatusCode { get; set; }
        [BsonElement]
        public string StatusName { get; set; }
        [BsonElement]
        public decimal Quantidade { get; set; }
        [BsonElement]
        public string Date { get; set; }
    }
}

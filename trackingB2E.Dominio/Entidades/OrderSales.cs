﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace trackingB2E.Dominio.Entidades
{
    public class OrderSales
    {

        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement]
        public string originApplication { get; set; }
        [BsonElement]
        public string originSalesOrderNumber { get; set; }
        [BsonElement]
        public string salesForceSalesOrderNumber { get; set; }
        [BsonElement]
        public string sapSalesOrderNumber { get; set; }
        [BsonElement]
        public List<Status> Status { get; set; }
        [BsonElement]
        public string Date { get; set; }
        [BsonElement]
        public List<Itens> itens { get; set; }
    }
}

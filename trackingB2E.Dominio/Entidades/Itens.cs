﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace trackingB2E.Dominio.Entidades
{
    public class Itens
    {
        [BsonElement]
        public string SAPSalesOrderNumber { get; set; }
        [BsonElement]
        public string ItemNumber { get; set; }
        [BsonElement]
        public List<Status> Status { get; set; }
        [BsonElement]
        public decimal Quantidade { get; set; }
        [BsonElement]
        public string Date { get; set; }

    }
}

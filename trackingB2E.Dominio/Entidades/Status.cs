﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace trackingB2E.Dominio.Entidades
{
    public class Status
    {
        [BsonElement]
        public string StatusCode { get; set; }
        [BsonElement]
        public string StatusName { get; set; }
        [BsonElement]
        public string Date { get; set; }
    }
}

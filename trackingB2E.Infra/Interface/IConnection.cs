﻿using System;
using System.Collections.Generic;
using System.Text;

namespace trackingB2E.Infra.Interface
{
    public interface IConnection
    {
        string OrderSalesCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}

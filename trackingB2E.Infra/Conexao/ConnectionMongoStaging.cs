﻿using System;
using System.Collections.Generic;
using System.Text;
using trackingB2E.Infra.Interface;

namespace trackingB2E.Infra.Conexao
{
    public class ConnectionMongoStaging : IConnection
    {
        public string OrderSalesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}

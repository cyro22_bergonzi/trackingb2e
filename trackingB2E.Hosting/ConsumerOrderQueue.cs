﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using trackingB2E.Servicos.Interfaces;

namespace trackingB2E.Hosting
{
    public class ConsumerOrderQueue : IHostedService
    {
        private IConsumeServico _consumeServico;
        private Timer _timer;


        public ConsumerOrderQueue(IConsumeServico consumeServico)
        {
            _consumeServico = consumeServico;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.FromMinutes(30), TimeSpan.FromHours(1));
            Console.WriteLine($"Consumo da fila orderSAP iniciado em {DateTime.UtcNow:o}");
            
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            _consumeServico.ConsumeSAPOrderFila();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine($"Consumo da fila orderSAP parou em {DateTime.UtcNow:o}");
            return Task.CompletedTask;
        }
    }
}

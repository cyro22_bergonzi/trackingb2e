﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using trackingB2E.Servicos.Interfaces;

namespace trackingB2E.Hosting
{
    public class ConsumerOrderItensQueue : IHostedService
    {
        private IConsumeServico _consumeServico;
        private Timer _timer;
        public ConsumerOrderItensQueue(IConsumeServico consumeServico)
        {
            _consumeServico = consumeServico;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromMinutes(5));
            Console.WriteLine($"Consumo da fila orderItensSAP iniciado em {DateTime.UtcNow:o}");
            
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            _consumeServico.ConsumeSAPOrderItensFila();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine($"Consumo da fila orderItensSAP parou em {DateTime.UtcNow:o}");
            return Task.CompletedTask;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime.CredentialManagement;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using trackingB2E.Infra.Conexao;
using trackingB2E.Infra.Interface;
using trackingB2E.Repositorio.Interfaces;
using trackingB2E.Repositorio.Repositorio;
using trackingB2E.Servicos.Interfaces;
using trackingB2E.Servicos.Servicos;

namespace trackingB2E
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMvc();

            services.AddTransient<IOrderServico, OrderServico>();
            services.AddTransient<IItensServico, ItensServico>();
            services.AddTransient<IOrderSalesServico, OrderSalesServico>();
            services.AddTransient<IConsumeServico, ConsumeServico>();
            services.AddTransient<IRepositorio, OrderSalesRepositorio>();
            services.AddTransient<IConsumeRepositorio, OrderSalesConsumeRepositorio>();
            services.AddTransient<IConnection, ConnectionMongoDEV>();
            /*
            services.AddTransient<IConnection, ConnectionMongoStaging>();
            services.AddTransient<IConnection, ConnectionMongoProd>();
            */
            // DEV
            services.Configure<ConnectionMongoDEV>(Configuration.GetSection(nameof(ConnectionMongoDEV)));
            services.AddSingleton<IConnection>(x => x.GetRequiredService<IOptions<ConnectionMongoDEV>>().Value);
            // Staging
            /*
            services.Configure<ConnectionMongoStaging>(Configuration.GetSection(nameof(ConnectionMongoStaging)));
            services.AddSingleton<IConnection>(x => x.GetRequiredService<IOptions<ConnectionMongoStaging>>().Value);
            // PROD
            services.Configure<ConnectionMongoProd>(Configuration.GetSection(nameof(ConnectionMongoProd)));
            services.AddSingleton<IConnection>(x => x.GetRequiredService<IOptions<ConnectionMongoProd>>().Value);
            */

            services.AddSingleton<OrderServico>();
            services.AddSingleton<ItensServico>();
            services.AddSingleton<OrderSalesServico>();
            services.AddSingleton<ConsumeServico>();
            services.AddSingleton<OrderSalesRepositorio>();

            services.AddSwaggerGen(x =>
           {
               x.SwaggerDoc("v1", new OpenApiInfo { Title = "Duratex - B2E", Version = "v1", });

           });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();

            app.UseSwaggerUI(x =>
           {
               x.RoutePrefix = string.Empty;
               x.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
           });
        }
    }
}

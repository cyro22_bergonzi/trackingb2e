﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using trackingB2E.Dominio.Entidades;
using trackingB2E.Servicos.Interfaces;

namespace trackingB2E.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderSalesController : ControllerBase
    {
        private readonly ILogger<OrderController> _logger;
        private readonly IOrderSalesServico _orderSalesServico;


        public OrderSalesController(ILogger<OrderController> logger, IOrderSalesServico orderSalesServico)
        {
            _logger = logger;
            _orderSalesServico = orderSalesServico;
        }


        // POST Grava Entidade OrderSalesForce na Fila
        [HttpPost]
        [Route("filaSalesOrder")]
        public IActionResult InsertOrderSales([FromBody] OrderSales order)
        {
            try
            {
                _orderSalesServico.SetOrderSalesFila(order);
                return new StatusCodeResult(200);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar publicar na fila", ex);
                return new StatusCodeResult(500);
            }

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using trackingB2E.Dominio.Entidades;
using trackingB2E.Servicos.Interfaces;

namespace trackingB2E.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderItensController : ControllerBase
    {
        private readonly ILogger<OrderController> _logger;
        private readonly IItensServico _itensServico;

        public OrderItensController(ILogger<OrderController> logger, IItensServico itensServico)
        {
            _logger = logger;
            _itensServico = itensServico;
        }

        // POST Grava Entidade ItensSAP na Fila
        [HttpPost]
        [Route("fila")]
        public IActionResult InsertItensSAP([FromBody] SapSalesOrderItem itens)
        {
            try
            {
                _itensServico.SetItensSAPFila(itens);
                return new StatusCodeResult(200);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar publicar na fila", ex);
                return new StatusCodeResult(500);
            }

        }
    }
}

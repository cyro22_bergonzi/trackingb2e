﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using trackingB2E.Dominio.Entidades;
using trackingB2E.Servicos.Interfaces;

namespace trackingB2E.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly ILogger<OrderController> _logger;
        private readonly IOrderServico _orderServico;

        public OrderController(ILogger<OrderController> logger, IOrderServico orderServico)
        {
            _logger = logger;
            _orderServico = orderServico;
        }

        [HttpGet]
        [Route("")]
        public ActionResult Get()
        {
            return new StatusCodeResult(200);
        }


        //POST Grava Entidade OrderSAP na Fila
        [HttpPost]
        [Route("fila")]
        public IActionResult InsertOrderSAP([FromBody] SapSalesOrderHeader sapOrderHeader)
        {
            try
            {
                _orderServico.SetHeaderOrderSAPFila(sapOrderHeader);
                return new StatusCodeResult(200);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar publicar na fila", ex);
                return new StatusCodeResult(500);
            }

        }

    }
}

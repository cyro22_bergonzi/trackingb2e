﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Amazon.Runtime.Internal.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using Newtonsoft.Json;
using trackingB2E.Dominio.Entidades;
using trackingB2E.Servicos.Interfaces;

namespace trackingB2E.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsumeController : ControllerBase
    {
        private readonly ILogger<ConsumeController> _logger;
        private readonly IConsumeServico _consumeServico;
        private readonly IOrderSalesServico _orderSalesServico;

        public ConsumeController(ILogger<ConsumeController> logger, IConsumeServico consumeServico, IOrderSalesServico orderSalesServico)
        {
            _logger = logger;
            _consumeServico = consumeServico;
            _orderSalesServico = orderSalesServico;
        }

        // GET api/status
        [HttpGet]
        [Route("sales/order/{orderId}")]
        public ActionResult<OrderSales> GetOrderClient(string orderId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId))
                {
                    var detalheExcecao = new Exception("É necessário passar o parâmetro Id de pedido do cliente!");

                    throw detalheExcecao;
                }

                return  _orderSalesServico.GetOrderSalesByClientOrderId(orderId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        [HttpGet]
        [Route("sales/ordersap/{orderId}")]
        public ActionResult<OrderSales> GetOrderSAP(string orderId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId))
                {
                    var detalheExcecao = new Exception("É necessário passar o parâmetro Id de pedido do SAP!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetOrderSalesBySapOrderId(orderId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        [HttpGet]
        [Route("sales/orderSalesForce/{orderId}")]
        public ActionResult<OrderSales> GetOrderSalesForce(string orderId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId))
                {
                    var detalheExcecao = new Exception("É necessário passar o parâmetro Id de pedido do SalesForce!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetOrderSalesBySalesForceOrderId(orderId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }




        [HttpGet]
        [Route("sales/orderClient/{orderId}/header/status")]
        public ActionResult<Status> GetOrderClientHeaderStatus(string orderId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId))
                {
                    var detalheExcecao = new Exception("É necessário passar o parâmetro Id de pedido do cliente!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetHeaderOrderSalesByClientOrderId(orderId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        [HttpGet]
        [Route("sales/orderSAP/{orderId}/header/status")]
        public ActionResult<Status> GetOrderSAPtHeaderStatus(string orderId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId))
                {
                    var detalheExcecao = new Exception("É necessário passar o parâmetro Id de pedido do SAP!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetHeaderOrderSalesBySapOrderId(orderId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }


        [HttpGet]
        [Route("sales/orderSalesForce/{orderId}/header/status")]
        public ActionResult<Status> GetOrderSalesForceHeaderStatus(string orderId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId))
                {
                    var detalheExcecao = new Exception("É necessário passar o parâmetro Id de pedido do SalesForce!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetHeaderOrderSalesBySalesForceOrderId(orderId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }



        [HttpGet]
        [Route("sales/orderClient/{orderId}/itens")]
        public ActionResult<List<Itens>> GetItensOrderClient(string orderId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId))
                {
                    var detalheExcecao = new Exception("É necessário passar o parâmetro Id de pedido do cliente!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetAllItensOrderSalesByClientOrderId(orderId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }


        [HttpGet]
        [Route("sales/orderSAP/{orderId}/itens")]
        public ActionResult<List<Itens>> GetItensOrderSAP(string orderId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId))
                {
                    var detalheExcecao = new Exception("É necessário passar o parâmetro Id de pedido do SAP!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetAllItensOrderSalesBySapOrderId(orderId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }



        [HttpGet]
        [Route("sales/orderSalesForce/{orderId}/itens")]
        public ActionResult<List<Itens>> GetItensOrderSalesForce(string orderId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId))
                {
                    var detalheExcecao = new Exception("É necessário passar o parâmetro Id de pedido do SalesForce!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetAllItensOrderSalesBySalesForceOrderId(orderId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }


        [HttpGet]
        [Route("sales/orderClient/{orderId}/item/{itemId}")]
        public ActionResult<Itens> GetItemOrderClient(string orderId, string itemId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId) || string.IsNullOrEmpty(itemId))
                {
                    var detalheExcecao = new Exception("É necessário passar os parâmetros de Id do item e Id de pedido do cliente!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetItensOrderSalesByClientOrderId(orderId, itemId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }


        [HttpGet]
        [Route("sales/orderSAP/{orderId}/item/{itemId}")]
        public ActionResult<Itens> GetItemOrderSAP([FromRoute] string orderId, string itemId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId) || string.IsNullOrEmpty(itemId))
                {
                    var detalheExcecao = new Exception("É necessário passar os parâmetros de Id do item e Id de pedido do SAP!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetItensOrderSalesBySapOrderId(orderId, itemId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }


        [HttpGet]
        [Route("sales/orderSalesForce/{orderId}/item/{itemId}")]
        public ActionResult<Itens> GetItemOrderSalesForce(string orderId, string itemId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId) || string.IsNullOrEmpty(itemId))
                {
                    var detalheExcecao = new Exception("É necessário passar os parâmetros de Id do item e Id de pedido do SalesForce!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetItensOrderSalesBySalesForceOrderId(orderId, itemId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }


        [HttpGet]
        [Route("sales/orderClient/{orderId}/item/{itemId}/status")]
        public ActionResult<Status> GetStatusItemOrderClient(string orderId, string orderItemId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId) || string.IsNullOrEmpty(orderItemId))
                {
                    var detalheExcecao = new Exception("É necessário passar os parâmetros de Id do item e Id de pedido do cliente!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetLastStatusItensOrderSalesByClientOrderId(orderId, orderItemId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        [HttpGet]
        [Route("sales/orderSAP/{orderId}/item/{itemId}/status")]
        public ActionResult<Status> GetStatusItemOrderSAP(string orderId, string itemId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId) || string.IsNullOrEmpty(itemId))
                {
                    var detalheExcecao = new Exception("É necessário passar os parâmetros de Id do item e Id de pedido do SAP!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetLastStatusItensOrderSalesBySapOrderId(orderId, itemId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        [HttpGet]
        [Route("sales/orderSalesForce/{orderId}/item/{itemId}/status")]
        public ActionResult<Status> GetStatusItemOrderSalesForce(string orderId, string itemId)
        {
            try
            {
                if (string.IsNullOrEmpty(orderId) || string.IsNullOrEmpty(itemId))
                {
                    var detalheExcecao = new Exception("É necessário passar os parâmetros de Id do item e Id de pedido do SalesForce!");

                    throw detalheExcecao;
                }

                return _orderSalesServico.GetLastStatusItensOrderSalesBySalesForceOrderId(orderId, itemId);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }
                



        [HttpPost]
        [Route("filaOrder")]
        public IActionResult ConsumeSAPFila()
        {
            try
            {
                _consumeServico.ConsumeSAPOrderFila();
                return new StatusCodeResult(200);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir a fila SAP", ex);
                return new StatusCodeResult(500);
            }

        }

        [HttpPost]
        [Route("filaOrderItens")]
        public IActionResult ConsumeSAPItensFila()
        {
            try
            {
                _consumeServico.ConsumeSAPOrderItensFila();
                return new StatusCodeResult(200);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir a fila de itens SAP", ex);
                return new StatusCodeResult(500);
            }

        }

        [HttpPost]
        [Route("filaOrderSales")]
        public IActionResult ConsumeOrderSalesFila()
        {
            try
            {
                _consumeServico.ConsumeOrderSalesFila();
                return new StatusCodeResult(200);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir a fila order sales", ex);
                return new StatusCodeResult(500);
            }

        }
    }
}

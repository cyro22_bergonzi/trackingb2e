﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using trackingB2E.Dominio.Entidades;
using trackingB2E.Repositorio.Interfaces;
using trackingB2E.Infra.Interface;
using System.Linq;
using System.Runtime.CompilerServices;

namespace trackingB2E.Repositorio.Repositorio
{
    public class OrderSalesConsumeRepositorio : IConsumeRepositorio
    {
        private readonly IMongoCollection<OrderSales> _mongoCollection;

        public OrderSalesConsumeRepositorio(IConnection connection)
        {
            var client = new MongoClient(connection.ConnectionString);
            var database = client.GetDatabase(connection.DatabaseName);
            _mongoCollection = database.GetCollection<OrderSales>(connection.OrderSalesCollectionName);
        }

        #region Retorna Entidade
        public OrderSales GetOrderSalesByClientOrderId(string orderId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("originSalesOrderNumber", orderId);
                          
                var retorno = _mongoCollection.Find(filter).FirstOrDefault();
                
                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o pedido do cliente - {orderId}");

                throw detalheExcecao;
            }
        }

        public OrderSales GetOrderSalesBySalesForceOrderId(string orderId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("salesForceSalesOrderNumber", orderId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault();

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o pedido do SalesForce - {orderId}");

                throw detalheExcecao;
            }
        }

        public OrderSales GetOrderSalesBySapOrderId(string orderId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", orderId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault();

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o pedido do SAP - {orderId}");

                throw detalheExcecao;
            }
        }
        #endregion

        #region Header
        public Status GetHeaderOrderSalesByClientOrderId(string orderId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("originSalesOrderNumber", orderId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault();

                return retorno.Status.OrderByDescending(x => x.Date).FirstOrDefault();
                
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o header do pedido cliente - {orderId}");

                throw detalheExcecao;
            }
        }
        public Status GetHeaderOrderSalesBySapOrderId(string orderId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", orderId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault();

                return retorno.Status.OrderByDescending(x => x.Date).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o header do pedido SAP - {orderId}");

                throw detalheExcecao;
            }
        }
        public Status GetHeaderOrderSalesBySalesForceOrderId(string orderId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", orderId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault();

                return retorno.Status.OrderByDescending(x => x.Date).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o header do pedido SalesForce - {orderId}");

                throw detalheExcecao;
            }
        }
        #endregion

        #region Itens

        #region All Itens
        public List<Itens> GetAllItensOrderSalesByClientOrderId(string orderId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("originSalesOrderNumber", orderId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault().itens;

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os itens do pedido {orderId} cliente");

                throw detalheExcecao;
            }
        }

        public List<Itens> GetAllItensOrderSalesBySapOrderId(string orderId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", orderId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault().itens;

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os itens do pedido {orderId} SAP");

                throw detalheExcecao;
            }

        }

        public List<Itens> GetAllItensOrderSalesBySalesForceOrderId(string orderId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("salesForceSalesOrderNumber", orderId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault().itens;

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os itens do pedido {orderId} SalesForce");

                throw detalheExcecao;
            }

        }
        #endregion

        #region All Status Item
        public List<Status> GetAllStatusItensOrderSalesByClientOrderId(string orderId, string orderItemId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("originSalesOrderNumber", orderId)
                           & Builders<OrderSales>.Filter.Eq("itens.itemNumber", orderItemId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault().Status;

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os status do item {orderItemId} do pedido {orderId} cliente");

                throw detalheExcecao;
            }
        }

        public List<Status> GetAllStatusItensOrderSalesBySapOrderId(string orderId, string orderItemId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", orderId)
                           & Builders<OrderSales>.Filter.Eq("itens.itemNumber", orderItemId); 

                var retorno = _mongoCollection.Find(filter).FirstOrDefault().Status;

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os status do item {orderItemId} do pedido {orderId} SAP");

                throw detalheExcecao;
            }
        }

        public List<Status> GetAllStatusItensOrderSalesBySalesForceOrderId(string orderId, string orderItemId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("salesForceSalesOrderNumber", orderId) 
                           & Builders<OrderSales>.Filter.Eq("itens.itemNumber", orderItemId);


                var retorno = _mongoCollection.Find(filter).FirstOrDefault().Status;

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os status do item {orderItemId} do pedido {orderId} SalesForce");

                throw detalheExcecao;
            }
        }
        #endregion

        #region Get Item
        public Itens GetItensOrderSalesByClientOrderId(string orderId, string orderItemId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("originSalesOrderNumber", orderId)
                           & Builders<OrderSales>.Filter.Eq("itens.itemNumber", orderItemId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault().itens.FirstOrDefault();

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os status do item {orderItemId} do pedido {orderId} cliente");

                throw detalheExcecao;
            }
        }

        public Itens GetItensOrderSalesBySapOrderId(string orderId, string orderItemId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", orderId);
                
                var retorno = _mongoCollection.Find(filter).FirstOrDefault().itens.Where(x => x.ItemNumber == orderItemId).FirstOrDefault();

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os status do item {orderItemId} do pedido {orderId} SAP");

                throw detalheExcecao;
            }
        }

        public Itens GetItensOrderSalesBySalesForceOrderId(string orderId, string orderItemId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("salesForceSalesOrderNumber", orderId)
                           & Builders<OrderSales>.Filter.Eq("itens.itemNumber", orderItemId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault().itens.FirstOrDefault();

                return retorno;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os status do item {orderItemId} do pedido {orderId} SalesForce");

                throw detalheExcecao;
            }
        }
        #endregion

        #region Last Status Item
        public Status GetLastStatusItensOrderSalesByClientOrderId(string orderId, string orderItemId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("originSalesOrderNumber", orderId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault().itens.Where(x => x.ItemNumber == orderItemId).FirstOrDefault().Status;

                return retorno.OrderByDescending(x => x.Date).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os status do item {orderItemId} do pedido {orderId} cliente");

                throw detalheExcecao;
            }
        }

        public Status GetLastStatusItensOrderSalesBySapOrderId(string orderId, string orderItemId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", orderId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault().itens.Where(x => x.ItemNumber == orderItemId).FirstOrDefault().Status;

                return retorno.OrderByDescending(x => x.Date).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os status do item {orderItemId} do pedido {orderId} SAP");

                throw detalheExcecao;
            }
        }

        public Status GetLastStatusItensOrderSalesBySalesForceOrderId(string orderId, string orderItemId)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("salesForceSalesOrderNumber", orderId);

                var retorno = _mongoCollection.Find(filter).FirstOrDefault().itens.Where(x => x.ItemNumber == orderItemId).FirstOrDefault().Status;

                return retorno.OrderByDescending(x => x.Date).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os status do item {orderItemId} do pedido {orderId} SalesForce");

                throw detalheExcecao;
            }
        }
        #endregion

        #endregion
    }
}

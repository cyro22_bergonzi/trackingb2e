﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using trackingB2E.Dominio.Entidades;
using trackingB2E.Infra.Interface;
using trackingB2E.Repositorio.Interfaces;

namespace trackingB2E.Repositorio.Repositorio
{
    public class OrderSalesRepositorio : IRepositorio
    {
        private readonly IMongoCollection<OrderSales> _mongoCollection;
        
        public OrderSalesRepositorio(IConnection connection)
        {
            var client = new MongoClient(connection.ConnectionString);
            var database = client.GetDatabase(connection.DatabaseName);
            _mongoCollection = database.GetCollection<OrderSales>(connection.OrderSalesCollectionName);
        }
        public async Task<OrderSales> BuscaOrderExistente(OrderSales order)
        {
            try
            {
                var resultado = new OrderSales();
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", order.sapSalesOrderNumber)
                & Builders<OrderSales>.Filter.Eq("salesForceSalesOrderNumber", order.salesForceSalesOrderNumber)
                & Builders<OrderSales>.Filter.Eq("originSalesOrderNumber", order.originSalesOrderNumber);
                
                resultado = await _mongoCollection.Find(filter).FirstOrDefaultAsync();
                return resultado;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o objeto - {order.sapSalesOrderNumber}");

                throw detalheExcecao;

            }
        }

        public async Task<OrderSales> BuscaOrderStatusExistente(OrderSales order)
        {
            try
            {
                var resultado = new OrderSales();
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", order.sapSalesOrderNumber)
                & Builders<OrderSales>.Filter.Eq("salesForceSalesOrderNumber", order.salesForceSalesOrderNumber)
                & Builders<OrderSales>.Filter.Eq("originSalesOrderNumber", order.originSalesOrderNumber)
                & Builders<OrderSales>.Filter.Eq("Status.StatusCode", order.Status.FirstOrDefault().StatusCode);


                resultado = await _mongoCollection.Find(filter).FirstOrDefaultAsync();
                return resultado;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o status do objeto - {order.sapSalesOrderNumber}");

                throw detalheExcecao;

            }
        }

        public async Task<OrderSales> BuscaOrderItemExistente(OrderItem item)
        {
            try
            {
                var resultado = new OrderSales();
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", item.SAPSalesOrderNumber)
                & Builders<OrderSales>.Filter.Eq("itens.ItemNumber", item.ItemNumber);

                resultado = await _mongoCollection.Find(filter).FirstOrDefaultAsync();
                return resultado;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o item do objeto - {item.SAPSalesOrderNumber}");

                throw detalheExcecao;

            }
        }

        public async Task<OrderSales> BuscaOrderItemStatusExistente(OrderItem item)
        {
            try
            {
                var resultado = new OrderSales();
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", item.SAPSalesOrderNumber)
                & Builders<OrderSales>.Filter.Eq("itens.ItemNumber", item.ItemNumber)
                & Builders<OrderSales>.Filter.Eq("itens.Status.StatusCode", item.StatusCode);

                resultado = await _mongoCollection.Find(filter).FirstOrDefaultAsync();
                return resultado;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o status do item {item.ItemNumber} do objeto - {item.SAPSalesOrderNumber}");

                throw detalheExcecao;

            }
        }

        public bool InsertOrderSalesMongo(OrderSales order)
        {
            try
            {
                var retorno = _mongoCollection.InsertOneAsync(order);

                return retorno.Exception == null ? true : false;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao tentar inserir os dados - {order}");

                throw detalheExcecao;
            }

        }


        public bool AtualizarOrderSalesMongo(OrderSales order)
        {
            try
            {


                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", order.sapSalesOrderNumber)
                & Builders<OrderSales>.Filter.Eq("salesForceSalesOrderNumber", order.salesForceSalesOrderNumber)
                & Builders<OrderSales>.Filter.Eq("originSalesOrderNumber", order.originSalesOrderNumber);
                
                var update = Builders<OrderSales>.Update.PushEach("Status", order.Status);
                
                var retorno = _mongoCollection.UpdateOneAsync(filter, update).Result.ModifiedCount > 0;

                return retorno;
                
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao tentar atualizar os dados - {order}");

                throw detalheExcecao;
            }
        }

        //Chamado quando a fila de Itens do pedido é consumida
        public bool AtualizarOrderSalesItemMongo(OrderSales orderItem)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", orderItem.sapSalesOrderNumber);
                
                var update = Builders<OrderSales>.Update.Push("itens", orderItem.itens);

                var retorno = _mongoCollection.UpdateOneAsync(filter, update).Result.ModifiedCount > 0;

                return retorno;

            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao tentar atualizar os dados - {orderItem.sapSalesOrderNumber}");

                throw detalheExcecao;
            }
        }

        //Chamado quando a fila de Itens do pedido é consumida
        public bool AtualizarOrderSalesItemStatusMongo(OrderSales orderItem)
        {
            try
            {
                var filter = Builders<OrderSales>.Filter.Eq("sapSalesOrderNumber", orderItem.sapSalesOrderNumber)
                           & Builders<OrderSales>.Filter.Eq("itens.ItemNumber", orderItem.itens.FirstOrDefault().ItemNumber);

                var st = new Status();
                st = orderItem.itens.FirstOrDefault().Status.FirstOrDefault();

                var update = Builders<OrderSales>.Update.Push("itens.$.Status", st);

                var retorno = _mongoCollection.UpdateOneAsync(filter, update);

                return true;

            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao tentar atualizar os dados - {orderItem.sapSalesOrderNumber}");

                throw detalheExcecao;
            }
        }

    }
}

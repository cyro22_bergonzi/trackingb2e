﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using trackingB2E.Dominio.Entidades;

namespace trackingB2E.Repositorio.Interfaces
{
    public interface IRepositorio
    {
        bool InsertOrderSalesMongo(OrderSales order);
        Task<OrderSales> BuscaOrderExistente(OrderSales order);
        Task<OrderSales> BuscaOrderStatusExistente(OrderSales order);
        Task<OrderSales> BuscaOrderItemExistente(OrderItem item);
        Task<OrderSales> BuscaOrderItemStatusExistente(OrderItem item);
        bool AtualizarOrderSalesMongo(OrderSales order);
        bool AtualizarOrderSalesItemMongo(OrderSales order);
        bool AtualizarOrderSalesItemStatusMongo(OrderSales order);
    }
}

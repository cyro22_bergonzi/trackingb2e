﻿using System;
using System.Collections.Generic;
using System.Text;
using trackingB2E.Dominio.Entidades;

namespace trackingB2E.Repositorio.Interfaces
{
    public interface IConsumeRepositorio
    {
        OrderSales GetOrderSalesByClientOrderId(string orderId);
        OrderSales GetOrderSalesBySapOrderId(string orderId);
        OrderSales GetOrderSalesBySalesForceOrderId(string orderId);

        Status GetHeaderOrderSalesByClientOrderId(string orderId);
        Status GetHeaderOrderSalesBySapOrderId(string orderId);
        Status GetHeaderOrderSalesBySalesForceOrderId(string orderId);

        List<Itens> GetAllItensOrderSalesByClientOrderId(string orderId);
        List<Itens> GetAllItensOrderSalesBySapOrderId(string orderId);
        List<Itens> GetAllItensOrderSalesBySalesForceOrderId(string orderId);

        List<Status> GetAllStatusItensOrderSalesByClientOrderId(string orderId, string orderItemId);
        List<Status> GetAllStatusItensOrderSalesBySapOrderId(string orderId, string orderItemId);
        List<Status> GetAllStatusItensOrderSalesBySalesForceOrderId(string orderId, string orderItemId);

        Itens GetItensOrderSalesByClientOrderId(string orderId, string orderItemId);
        Itens GetItensOrderSalesBySapOrderId(string orderId, string orderItemId);
        Itens GetItensOrderSalesBySalesForceOrderId(string orderId, string orderItemId);

        Status GetLastStatusItensOrderSalesByClientOrderId(string orderId, string orderItemId);
        Status GetLastStatusItensOrderSalesBySapOrderId(string orderId, string orderItemId);
        Status GetLastStatusItensOrderSalesBySalesForceOrderId(string orderId, string orderItemId);

    }
}
